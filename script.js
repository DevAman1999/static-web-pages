document.getElementById("contactForm").addEventListener("submit", function(event) {
            event.preventDefault(); // Prevent the default form submission
            
            // Send the form data using AJAX
            var formData = new FormData(this);
            fetch(this.action, {
                method: 'POST',
                body: formData
            })
            .then(response => {
                if (response.ok) {
                    // If the response is OK, show a pop-up message
                    alert("Thanks for the contact!");
                    // You can also reset the form if needed
                    // this.reset();
                } else {
                    throw new Error('Network response was not ok.');
                }
            })
            .catch(error => {
                console.error('Error:', error);
                alert('An error occurred while submitting the form.');
            });
        });
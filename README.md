Beginning my journey into web development, I'm diving into HTML and CSS to craft static web pages. 



1. My initial project? A **blog** showcasing my software development work to date, with plans for ongoing refinement and innovation. Additionally, there's a "Contact Me" section where you're welcome to reach out.
